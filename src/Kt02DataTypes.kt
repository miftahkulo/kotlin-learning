fun main(args: Array<String>) {
    // mutable : can to be changed
    var myString = "Andi"
    myString = "Bayu"
    println(myString)

    var myInt = 3
    var myAnotherInt = 10
    println("10/3:" + myAnotherInt/myInt)

    var myDecimal = 10.0
    println("10.0/3:" + myDecimal/myInt)

    // imutable : can not to be changed
    val myAnotherString = "caya"
    // myAnotherString = "cayaiswa" // error
    println(myAnotherString)
}
